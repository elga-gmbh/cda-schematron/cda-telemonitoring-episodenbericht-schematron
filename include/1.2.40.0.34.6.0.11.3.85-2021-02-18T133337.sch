<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.85
Name: Device Time Synchronization Information Observation
Description: Die Device Time Synchronization Information Observation dokumentiert die Zeit-Synchronisierung Methode des verwendeten Gerätes. In PCHA konformen Geräten, kommen die Informationen aus dem MdsTimeInfo Attribut.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337">
   <title>Device Time Synchronization Information Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]"
         id="d41e54955-false-d3609065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85']) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85']) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:value[@codeSystem = '2.16.840.1.113883.6.24']) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:value[@codeSystem = '2.16.840.1.113883.6.24'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:value[@codeSystem = '2.16.840.1.113883.6.24']) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:value[@codeSystem = '2.16.840.1.113883.6.24'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85']
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85']"
         id="d41e54961-false-d3609126e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.85')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.85' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']"
         id="d41e54970-false-d3609141e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.29')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.29' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e54981-false-d3609163e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@code) = ('68220')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von code MUSS '68220' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@displayName) = ('MDC_TIME_SYNC_PROTOCOL: Time synchronization protocol')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von displayName MUSS 'MDC_TIME_SYNC_PROTOCOL: Time synchronization protocol' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e54991-false-d3609201e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@code">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@codeSystem">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e55006-false-d3609228e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@language">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]"
         id="d41e55016-false-d3609244e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]"
         id="d41e55021-false-d3609260e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@value">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.85
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:value[@codeSystem = '2.16.840.1.113883.6.24']
Item: (atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:value[@codeSystem = '2.16.840.1.113883.6.24']"
         id="d41e55027-false-d3609274e0">
      <extends rule="CD"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="@code">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
   </rule>
</pattern>
