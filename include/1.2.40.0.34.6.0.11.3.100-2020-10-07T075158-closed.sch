<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.100
Name: Serienmessung Vitalparameter Entry
Description: Das Serienmessung Entry dokumentiert eine kontinuierliche Messungen eines Gerätes. Eine kontinuierliche Messung beinhaltet mehrere Datenpunkte und eine Zeitspanne. Diese Messwerte sind   als Vitalparameter definiert! 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158-closed">
   <title>Serienmessung Vitalparameter Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']])]"
         id="d41e33609-true-d2293203e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33609-true-d2293203e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']] (rule-reference: d41e33609-true-d2293203e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[@value] | self::hl7:effectiveTime[@nullFlavor='UNK'] | self::hl7:effectiveTime | self::hl7:value[@nullFlavor='NA'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]])]"
         id="d41e33708-true-d2293769e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33708-true-d2293769e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | hl7:id[not(@nullFlavor)] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[@value] | hl7:effectiveTime[@nullFlavor='UNK'] | hl7:effectiveTime | hl7:value[@nullFlavor='NA'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]] (rule-reference: d41e33708-true-d2293769e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation | self::ips:designation)]"
         id="d41e33756-true-d2293819e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33756-true-d2293819e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation | ips:designation (rule-reference: d41e33756-true-d2293819e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e33774-true-d2293833e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33774-true-d2293833e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e33774-true-d2293833e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e33813-true-d2293862e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33813-true-d2293862e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e33813-true-d2293862e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d41e33871-true-d2293894e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33871-true-d2293894e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d41e33871-true-d2293894e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d41e33944-true-d2294063e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33944-true-d2294063e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d41e33944-true-d2294063e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2293924e14-true-d2294169e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2293924e14-true-d2294169e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2293924e14-true-d2294169e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2294173e101-true-d2294242e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294173e101-true-d2294242e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2294173e101-true-d2294242e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2294173e174-true-d2294318e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2294173e174-true-d2294318e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2294173e174-true-d2294318e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2294173e186-true-d2294340e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2294173e186-true-d2294340e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2294173e186-true-d2294340e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2294328e12-true-d2294369e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2294328e12-true-d2294369e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2294328e12-true-d2294369e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2294173e198-true-d2294420e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294173e198-true-d2294420e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2294173e198-true-d2294420e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2294394e58-true-d2294481e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294394e58-true-d2294481e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2294394e58-true-d2294481e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d41e33946-true-d2294648e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33946-true-d2294648e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d41e33946-true-d2294648e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d2294526e52-true-d2294756e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294526e52-true-d2294756e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d2294526e52-true-d2294756e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2294526e71-true-d2294815e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294526e71-true-d2294815e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2294526e71-true-d2294815e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2294526e117-true-d2294872e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294526e117-true-d2294872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2294526e117-true-d2294872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2294876e92-true-d2294906e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d2294876e92-true-d2294906e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2294876e92-true-d2294906e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d2294526e140-true-d2294954e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294526e140-true-d2294954e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d2294526e140-true-d2294954e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2294526e156-true-d2294999e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294526e156-true-d2294999e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2294526e156-true-d2294999e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2294969e67-true-d2295062e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2294969e67-true-d2295062e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2294969e67-true-d2295062e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d41e33959-true-d2295230e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33959-true-d2295230e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d41e33959-true-d2295230e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2295107e5-true-d2295362e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295107e5-true-d2295362e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2295107e5-true-d2295362e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2295235e50-true-d2295428e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295235e50-true-d2295428e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2295235e50-true-d2295428e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2295235e120-true-d2295490e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295235e120-true-d2295490e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2295235e120-true-d2295490e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2295235e132-true-d2295512e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295235e132-true-d2295512e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2295235e132-true-d2295512e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2295500e12-true-d2295541e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295500e12-true-d2295541e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2295500e12-true-d2295541e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2295235e143-true-d2295592e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295235e143-true-d2295592e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2295235e143-true-d2295592e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2295566e58-true-d2295653e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295566e58-true-d2295653e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2295566e58-true-d2295653e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d2295107e7-true-d2295730e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295107e7-true-d2295730e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d2295107e7-true-d2295730e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2295107e16-true-d2295783e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295107e16-true-d2295783e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2295107e16-true-d2295783e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2295107e20-true-d2295838e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295107e20-true-d2295838e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2295107e20-true-d2295838e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2295831e10-true-d2295865e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295831e10-true-d2295865e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2295831e10-true-d2295865e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d41e33961-true-d2295993e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33961-true-d2295993e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d41e33961-true-d2295993e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d2295896e11-true-d2296101e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295896e11-true-d2296101e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d2295896e11-true-d2296101e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2295896e22-true-d2296163e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295896e22-true-d2296163e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2295896e22-true-d2296163e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d2295896e78-true-d2296241e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295896e78-true-d2296241e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d2295896e78-true-d2296241e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d2295896e80-true-d2296302e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295896e80-true-d2296302e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d2295896e80-true-d2296302e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d2295896e83-true-d2296360e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2295896e83-true-d2296360e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d2295896e83-true-d2296360e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']])]"
         id="d41e33963-true-d2296415e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33963-true-d2296415e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']] (rule-reference: d41e33963-true-d2296415e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | self::hl7:code[@nullFlavor='NA'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]])]"
         id="d2296383e6-true-d2296472e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296383e6-true-d2296472e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | hl7:code[@nullFlavor='NA'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]] (rule-reference: d2296383e6-true-d2296472e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']])]"
         id="d2296383e36-true-d2296510e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296383e36-true-d2296510e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']] (rule-reference: d2296383e36-true-d2296510e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | self::hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value)]"
         id="d2296492e7-true-d2296559e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296492e7-true-d2296559e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:value (rule-reference: d2296492e7-true-d2296559e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:translation | self::ips:designation)]"
         id="d2296492e31-true-d2296603e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296492e31-true-d2296603e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:translation | ips:designation (rule-reference: d2296492e31-true-d2296603e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2296492e48-true-d2296617e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296492e48-true-d2296617e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2296492e48-true-d2296617e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2296492e102-true-d2296652e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296492e102-true-d2296652e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2296492e102-true-d2296652e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:origin[not(@nullFlavor)] | self::hl7:scale[not(@nullFlavor)] | self::hl7:digits[not(@nullFlavor)])]"
         id="d2296492e119-true-d2296687e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296492e119-true-d2296687e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:origin[not(@nullFlavor)] | hl7:scale[not(@nullFlavor)] | hl7:digits[not(@nullFlavor)] (rule-reference: d2296492e119-true-d2296687e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']])]"
         id="d2296383e46-true-d2296725e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296383e46-true-d2296725e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']] (rule-reference: d2296383e46-true-d2296725e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | self::hl7:code[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)])]"
         id="d2296707e7-true-d2296765e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296707e7-true-d2296765e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | hl7:code[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] (rule-reference: d2296707e7-true-d2296765e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2296707e41-true-d2296796e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296707e41-true-d2296796e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2296707e41-true-d2296796e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:head[not(@nullFlavor)] | self::hl7:increment[not(@nullFlavor)])]"
         id="d2296707e53-true-d2296820e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2020-10-07T075158.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2296707e53-true-d2296820e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:head[not(@nullFlavor)] | hl7:increment[not(@nullFlavor)] (rule-reference: d2296707e53-true-d2296820e0)</assert>
   </rule>
</pattern>
