<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.13.3.8
Name: ELGA Medical Device
Description: Dieses Entry beschreibt das medizinisches Gerät oder Implantat, das vom Patienten zur Zeit verwendet wird.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.13.3.8-2017-02-26T111752">
   <title>ELGA Medical Device</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]"
         id="d41e284-false-d1947e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]) &gt;= 1">(ELGAMedicalDevice): Element hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]) &lt;= 1">(ELGAMedicalDevice): Element hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]"
         id="d41e316-false-d1999e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@classCode) = ('SPLY')">(ELGAMedicalDevice): Der Wert von classCode MUSS 'SPLY' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@moodCode) = ('EVN')">(ELGAMedicalDevice): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']) &gt;= 1">(ELGAMedicalDevice): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.8'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']) &lt;= 1">(ELGAMedicalDevice): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.8'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:text) &lt;= 1">(ELGAMedicalDevice): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:effectiveTime) &gt;= 1">(ELGAMedicalDevice): Element hl7:effectiveTime ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:effectiveTime) &lt;= 1">(ELGAMedicalDevice): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]) &gt;= 1">(ELGAMedicalDevice): Element hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]) &lt;= 1">(ELGAMedicalDevice): Element hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:reference[hl7:externalDocument]) &lt;= 1">(ELGAMedicalDevice): Element hl7:reference[hl7:externalDocument] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']"
         id="d41e322-false-d2083e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@root) = ('1.2.40.0.34.11.13.3.8')">(ELGAMedicalDevice): Der Wert von root MUSS '1.2.40.0.34.11.13.3.8' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:id
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:id"
         id="d41e327-false-d2099e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:text
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:text"
         id="d41e329-false-d2109e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(ELGAMedicalDevice): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(ELGAMedicalDevice): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d41e334-false-d2128e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="@value">(ELGAMedicalDevice): Attribut @value MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:effectiveTime
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:effectiveTime"
         id="d41e345-false-d2142e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:low) &gt;= 1">(ELGAMedicalDevice): Element hl7:low ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:low) &lt;= 1">(ELGAMedicalDevice): Element hl7:low kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:effectiveTime/hl7:low
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:effectiveTime/hl7:low"
         id="d41e354-false-d2161e0">
      <extends rule="IVXB_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVXB_TS')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVXB_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]
Item: (ELGAMedicalDevice)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@typeCode) = ('AUT') or not(@typeCode)">(AuthorBody_PS): Der Wert von typeCode MUSS 'AUT' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(AuthorBody_PS): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:time) &gt;= 1">(AuthorBody_PS): Element hl7:time ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:time) &lt;= 1">(AuthorBody_PS): Element hl7:time kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthor[hl7:representedOrganization]) &gt;= 1">(AuthorBody_PS): Element hl7:assignedAuthor[hl7:representedOrganization] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthor[hl7:representedOrganization]) &lt;= 1">(AuthorBody_PS): Element hl7:assignedAuthor[hl7:representedOrganization] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:time
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:time">
      <extends rule="TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="not(*)">(AuthorBody_PS): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:id) &gt;= 1">(AuthorBody_PS): Element hl7:id ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(AuthorBody_PS): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:assignedPerson | hl7:assignedAuthoringDevice)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="$elmcount &gt;= 1">(AuthorBody_PS): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="$elmcount &lt;= 1">(AuthorBody_PS): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedPerson) &lt;= 1">(AuthorBody_PS): Element hl7:assignedPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthoringDevice) &lt;= 1">(AuthorBody_PS): Element hl7:assignedAuthoringDevice kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:representedOrganization[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:representedOrganization[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:id
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:id">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(AuthorBody_PS): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.6 ELGA_AuthorSpeciality (DYNAMIC)' sein.</assert>
      <report fpi="CD-DEPR-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept[@type='D'][@code = $theCode][@codeSystem = $theCodeSystem])">(AuthorBody_PS): Element hl7:code ist codiert mit Bindungsstärke 'required' und enthält einen Code der veraltet ist.</report>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:addr
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:telecom
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:telecom">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90001
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]
Item: (PersonElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90001-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(PersonElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('DEV') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'DEV' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:manufacturerModelName) &gt;= 1">(AuthorBody_PS): Element hl7:manufacturerModelName ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:manufacturerModelName) &lt;= 1">(AuthorBody_PS): Element hl7:manufacturerModelName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:softwareName) &gt;= 1">(AuthorBody_PS): Element hl7:softwareName ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:softwareName) &lt;= 1">(AuthorBody_PS): Element hl7:softwareName kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:addr) &lt;= 1">(AuthorBody_PS): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant
Item: (ELGAMedicalDevice)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@typeCode) = ('INF') or not(@typeCode)">(InformantBodyPS): Der Wert von typeCode MUSS 'INF' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(InformantBodyPS): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="$elmcount &gt;= 1">(InformantBodyPS): Auswahl (hl7:assignedEntity  oder  hl7:relatedEntity[@classCode = 'PRS']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="$elmcount &lt;= 1">(InformantBodyPS): Auswahl (hl7:assignedEntity  oder  hl7:relatedEntity[@classCode = 'PRS']) enthält zu viele Elemente [max 1x]</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">(InformantBodyPS): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:id) &gt;= 1">(InformantBodyPS): Element hl7:id ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(InformantBodyPS): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:assignedPerson) &lt;= 1">(InformantBodyPS): Element hl7:assignedPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:representedOrganisation) &lt;= 1">(InformantBodyPS): Element hl7:representedOrganisation kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:id
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:id">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(InformantBodyPS): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.6 ELGA_AuthorSpeciality (DYNAMIC)' sein.</assert>
      <report fpi="CD-DEPR-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept[@type='D'][@code = $theCode][@codeSystem = $theCodeSystem])">(InformantBodyPS): Element hl7:code ist codiert mit Bindungsstärke 'required' und enthält einen Code der veraltet ist.</report>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:telecom
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:telecom">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(InformantBodyPS): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(InformantBodyPS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90001
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]
Item: (PersonElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90001-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(PersonElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(InformantBodyPS): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(InformantBodyPS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:addr) &lt;= 1">(InformantBodyPS): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:id
Item: (OrganizationElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:id">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:name[not(@nullFlavor)]
Item: (OrganizationElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:telecom
Item: (OrganizationElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:telecom">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:addr
Item: (OrganizationElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@classCode) = ('PRS')">(InformantBodyPS): Der Wert von classCode MUSS 'PRS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(InformantBodyPS): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:relatedPerson) &lt;= 1">(InformantBodyPS): Element hl7:relatedPerson kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(InformantBodyPS): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.17 ELGA_PersonalRelationship (DYNAMIC)' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:telecom
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:telecom">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(InformantBodyPS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.20
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson
Item: (InformantBodyPS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(InformantBodyPS): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(InformantBodyPS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.20-2017-08-10T204925.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(InformantBodyPS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90001
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]
Item: (PersonElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90001-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(PersonElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]"
         id="d41e380-false-d2985e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@typeCode) = ('DEV')">(ELGAMedicalDevice): Der Wert von typeCode MUSS 'DEV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:participantRole) &gt;= 1">(ELGAMedicalDevice): Element hl7:participantRole ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:participantRole) &lt;= 1">(ELGAMedicalDevice): Element hl7:participantRole kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole"
         id="d41e387-false-d3015e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@classCode) = ('MANU')">(ELGAMedicalDevice): Der Wert von classCode MUSS 'MANU' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:playingDevice) &gt;= 1">(ELGAMedicalDevice): Element hl7:playingDevice ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:playingDevice) &lt;= 1">(ELGAMedicalDevice): Element hl7:playingDevice kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:id
Item: (ELGAMedicalDevice)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice"
         id="d41e397-false-d3057e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@classCode) = ('DEV')">(ELGAMedicalDevice): Der Wert von classCode MUSS 'DEV' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="string(@determinerCode) = ('INSTANCE')">(ELGAMedicalDevice): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <report fpi="CD-UNKN-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:code) &lt; 1">(ELGAMedicalDevice): Element hl7:code ist codiert mit Bindungsstärke 'extensible' und enthält ein Code außerhalb des angegebene Wertraums.</report>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:code) &lt;= 1">(ELGAMedicalDevice): Element hl7:code kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code"
         id="d41e403-false-d3091e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:originalText) &lt;= 1">(ELGAMedicalDevice): Element hl7:originalText kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:translation) &lt;= 1">(ELGAMedicalDevice): Element hl7:translation kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText"
         id="d41e456-false-d3121e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:reference) &gt;= 1">(ELGAMedicalDevice): Element hl7:reference ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="count(hl7:reference) &lt;= 1">(ELGAMedicalDevice): Element hl7:reference kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/hl7:reference
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/hl7:reference"
         id="d41e458-false-d3140e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:translation
Item: (ELGAMedicalDevice)
-->

   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:participant[@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:translation"
         id="d41e465-false-d3150e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.8-2017-02-26T111752.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(ELGAMedicalDevice): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.8
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]
Item: (ELGAMedicalDevice)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.17
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]
Item: (ELGAExternalDocument)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.17
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument
Item: (ELGAExternalDocument)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="string(@classCode) = ('DOC') or not(@classCode)">(ELGAExternalDocument): Der Wert von classCode MUSS 'DOC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(ELGAExternalDocument): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(ELGAExternalDocument): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(ELGAExternalDocument): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="count(hl7:code) &lt;= 1">(ELGAExternalDocument): Element hl7:code kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="count(hl7:text) &lt;= 1">(ELGAExternalDocument): Element hl7:text kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.17
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:id[not(@nullFlavor)]
Item: (ELGAExternalDocument)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(ELGAExternalDocument): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.17
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:code
Item: (ELGAExternalDocument)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:code">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(ELGAExternalDocument): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.17
Context: *[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:text
Item: (ELGAExternalDocument)
-->
   <rule fpi="RULC-1"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/hl7:text">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.17-2017-08-09T105540.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ELGAExternalDocument): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
