<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.63
Name: Medikationsliste PS - kodiert
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537-closed">
   <title>Medikationsliste PS - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']])]"
         id="d41e24837-true-d1076849e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24837-true-d1076849e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']] (rule-reference: d41e24837-true-d1076849e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '10160-0' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]] | self::hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]] | self::hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d41e24889-true-d1077707e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24889-true-d1077707e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '10160-0' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]] | hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]] | hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d41e24889-true-d1077707e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d41e24931-true-d1077859e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24931-true-d1077859e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d41e24931-true-d1077859e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1077737e45-true-d1077967e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1077737e45-true-d1077967e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1077737e45-true-d1077967e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1077737e64-true-d1078026e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1077737e64-true-d1078026e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1077737e64-true-d1078026e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1077737e110-true-d1078083e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1077737e110-true-d1078083e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1077737e110-true-d1078083e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1078087e92-true-d1078117e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1078087e92-true-d1078117e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1078087e92-true-d1078117e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1077737e133-true-d1078165e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1077737e133-true-d1078165e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1077737e133-true-d1078165e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1077737e149-true-d1078210e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1077737e149-true-d1078210e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1077737e149-true-d1078210e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1078180e67-true-d1078273e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078180e67-true-d1078273e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1078180e67-true-d1078273e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d41e24937-true-d1078441e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24937-true-d1078441e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d41e24937-true-d1078441e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1078318e15-true-d1078573e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078318e15-true-d1078573e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1078318e15-true-d1078573e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1078446e50-true-d1078639e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078446e50-true-d1078639e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1078446e50-true-d1078639e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1078446e120-true-d1078701e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078446e120-true-d1078701e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1078446e120-true-d1078701e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1078446e132-true-d1078723e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078446e132-true-d1078723e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1078446e132-true-d1078723e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1078711e12-true-d1078752e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078711e12-true-d1078752e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1078711e12-true-d1078752e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1078446e143-true-d1078803e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078446e143-true-d1078803e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1078446e143-true-d1078803e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1078777e58-true-d1078864e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078777e58-true-d1078864e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1078777e58-true-d1078864e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1078318e17-true-d1078941e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078318e17-true-d1078941e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1078318e17-true-d1078941e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1078318e26-true-d1078994e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078318e26-true-d1078994e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1078318e26-true-d1078994e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1078318e30-true-d1079049e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1078318e30-true-d1079049e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1078318e30-true-d1079049e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1079042e10-true-d1079076e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079042e10-true-d1079076e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1079042e10-true-d1079076e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']])]"
         id="d41e24951-true-d1079286e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24951-true-d1079286e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']] (rule-reference: d41e24951-true-d1079286e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.21'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.1'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.9'] | self::hl7:id[1] | self::hl7:id[2] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[1][hl7:low] | self::hl7:effectiveTime[1][hl7:width] | self::hl7:effectiveTime[1][@nullFlavor] | self::hl7:effectiveTime[hl7:period and not(hl7:phase)] | self::hl7:effectiveTime[2] | self::hl7:effectiveTime[hl7:period and hl7:phase] | self::hl7:effectiveTime[hl7:comp] | self::hl7:effectiveTime[2] | self::hl7:doseQuantity[not(hl7:low|hl7:high)] | self::hl7:doseQuantity[hl7:low|hl7:high] | self::hl7:doseQuantity | self::hl7:doseQuantity[not(hl7:low|hl7:high)] | self::hl7:doseQuantity[hl7:low|hl7:high] | self::hl7:doseQuantity | self::hl7:repeatNumber | self::hl7:routeCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.4' or @nullFlavor] | self::hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]] | self::hl7:author[not(@nullFlavor)] | self::hl7:author[@nullFlavor] | self::hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']] | self::hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']] | self::hl7:entryRelationship[@typeCode='COMP'][hl7:supply][not(@nullFlavor)] | self::hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']] | self::hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']] | self::hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']] | self::hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument])]"
         id="d1079107e9-true-d1079472e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e9-true-d1079472e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.21'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.1'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.9'] | hl7:id[1] | hl7:id[2] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[1][hl7:low] | hl7:effectiveTime[1][hl7:width] | hl7:effectiveTime[1][@nullFlavor] | hl7:effectiveTime[hl7:period and not(hl7:phase)] | hl7:effectiveTime[2] | hl7:effectiveTime[hl7:period and hl7:phase] | hl7:effectiveTime[hl7:comp] | hl7:effectiveTime[2] | hl7:doseQuantity[not(hl7:low|hl7:high)] | hl7:doseQuantity[hl7:low|hl7:high] | hl7:doseQuantity | hl7:doseQuantity[not(hl7:low|hl7:high)] | hl7:doseQuantity[hl7:low|hl7:high] | hl7:doseQuantity | hl7:repeatNumber | hl7:routeCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.4' or @nullFlavor] | hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]] | hl7:author[not(@nullFlavor)] | hl7:author[@nullFlavor] | hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']] | hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']] | hl7:entryRelationship[@typeCode='COMP'][hl7:supply][not(@nullFlavor)] | hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']] | hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']] | hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']] | hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument] (rule-reference: d1079107e9-true-d1079472e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1079107e140-true-d1079552e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e140-true-d1079552e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1079107e140-true-d1079552e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[1][hl7:low]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1079567e31-true-d1079587e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30006-2013-12-20T000000.html"
              test="not(.)">(Einnahmedauer)/d1079567e31-true-d1079587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1079567e31-true-d1079587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[1][hl7:width]/*[not(@xsi:nil = 'true')][not(self::hl7:width)]"
         id="d1079567e57-true-d1079609e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30006-2013-12-20T000000.html"
              test="not(.)">(Einnahmedauer)/d1079567e57-true-d1079609e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:width (rule-reference: d1079567e57-true-d1079609e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:period and not(hl7:phase)]/*[not(@xsi:nil = 'true')][not(self::hl7:period)]"
         id="d1079621e48-true-d1079631e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30007-2014-09-02T000000.html"
              test="not(.)">(Dosierung1)/d1079621e48-true-d1079631e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:period (rule-reference: d1079621e48-true-d1079631e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:period and hl7:phase]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1079645e78-true-d1079664e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1079645e78-true-d1079664e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1079645e78-true-d1079664e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/*[not(@xsi:nil = 'true')][not(self::hl7:comp[not(@operator)] | self::hl7:comp[@operator='I'])]"
         id="d1079645e103-true-d1079683e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1079645e103-true-d1079683e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:comp[not(@operator)] | hl7:comp[@operator='I'] (rule-reference: d1079645e103-true-d1079683e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/hl7:comp[not(@operator)]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1079645e119-true-d1079700e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1079645e119-true-d1079700e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1079645e119-true-d1079700e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/hl7:comp[@operator='I']/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1079645e138-true-d1079727e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1079645e138-true-d1079727e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1079645e138-true-d1079727e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:doseQuantity[hl7:low|hl7:high]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1079746e38-true-d1079768e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30037-2015-09-05T000000.html"
              test="not(.)">(Dosierung1dq)/d1079746e38-true-d1079768e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1079746e38-true-d1079768e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:doseQuantity[hl7:low|hl7:high]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1079789e38-true-d1079811e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30041-2015-09-05T000000.html"
              test="not(.)">(Dosierung3dq)/d1079789e38-true-d1079811e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1079789e38-true-d1079811e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']])]"
         id="d1079107e291-true-d1079862e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e291-true-d1079862e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']] (rule-reference: d1079107e291-true-d1079862e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | self::hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']])]"
         id="d1079844e24-true-d1079896e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e24-true-d1079896e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']] (rule-reference: d1079844e24-true-d1079896e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | self::hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] | self::hl7:name | self::pharm:formCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.5' or @nullFlavor] | self::pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']] | self::pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']])]"
         id="d1079844e38-true-d1079962e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e38-true-d1079962e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] | hl7:name | pharm:formCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.5' or @nullFlavor] | pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']] | pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']] (rule-reference: d1079844e38-true-d1079962e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation[@codeSystem = '1.2.40.0.34.4.17' or @nullFlavor])]"
         id="d1079844e54-true-d1079996e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e54-true-d1079996e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation[@codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] (rule-reference: d1079844e54-true-d1079996e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']]/*[not(@xsi:nil = 'true')][not(self::pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE'])]"
         id="d1079844e232-true-d1080032e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e232-true-d1080032e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE'] (rule-reference: d1079844e232-true-d1080032e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']]/pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE']/*[not(@xsi:nil = 'true')][not(self::pharm:capacityQuantity[not(@nullFlavor)])]"
         id="d1079844e245-true-d1080046e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e245-true-d1080046e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:capacityQuantity[not(@nullFlavor)] (rule-reference: d1079844e245-true-d1080046e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/*[not(@xsi:nil = 'true')][not(self::pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND'])]"
         id="d1079844e278-true-d1080065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e278-true-d1080065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND'] (rule-reference: d1079844e278-true-d1080065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND']/*[not(@xsi:nil = 'true')][not(self::pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor] | self::pharm:name[not(@nullFlavor)])]"
         id="d1079844e314-true-d1080086e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e314-true-d1080086e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor] | pharm:name[not(@nullFlavor)] (rule-reference: d1079844e314-true-d1080086e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND']/pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1079844e323-true-d1080106e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079844e323-true-d1080106e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1079844e323-true-d1080106e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time | self::hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization])]"
         id="d1080125e133-true-d1080159e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e133-true-d1080159e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time | hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization] (rule-reference: d1080125e133-true-d1080159e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization[not(@nullFlavor)])]"
         id="d1080125e180-true-d1080220e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e180-true-d1080220e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:telecom | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization[not(@nullFlavor)] (rule-reference: d1080125e180-true-d1080220e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1080125e244-true-d1080253e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e244-true-d1080253e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1080125e244-true-d1080253e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1080125e255-true-d1080279e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e255-true-d1080279e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1080125e255-true-d1080279e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1080125e277-true-d1080310e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e277-true-d1080310e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1080125e277-true-d1080310e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[@nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:time[@nullFlavor = 'NA'] | self::hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']])]"
         id="d1080125e284-true-d1080349e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e284-true-d1080349e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time[@nullFlavor = 'NA'] | hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']] (rule-reference: d1080125e284-true-d1080349e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:author[@nullFlavor]/hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@nullFlavor = 'NA'])]"
         id="d1080125e302-true-d1080368e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1080125e302-true-d1080368e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@nullFlavor = 'NA'] (rule-reference: d1080125e302-true-d1080368e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/*[not(@xsi:nil = 'true')][not(self::hl7:sequenceNumber[not(@nullFlavor)] | self::hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable])]"
         id="d1080377e11-true-d1080391e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30040-2020-07-21T133602.html"
              test="not(.)">(Dosierung2er)/d1080377e11-true-d1080391e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:sequenceNumber[not(@nullFlavor)] | hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable] (rule-reference: d1080377e11-true-d1080391e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/*[not(@xsi:nil = 'true')][not(self::hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)] | self::hl7:doseQuantity[not(@nullFlavor)] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct])]"
         id="d1080400e8-true-d1080418e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1080400e8-true-d1080418e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)] | hl7:doseQuantity[not(@nullFlavor)] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] (rule-reference: d1080400e8-true-d1080418e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:event[not(@nullFlavor)] | self::hl7:offset[not(@nullFlavor)])]"
         id="d1080400e17-true-d1080435e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1080400e17-true-d1080435e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:event[not(@nullFlavor)] | hl7:offset[not(@nullFlavor)] (rule-reference: d1080400e17-true-d1080435e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1080400e77-true-d1080464e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1080400e77-true-d1080464e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1080400e77-true-d1080464e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1080400e79-true-d1080478e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1080400e79-true-d1080478e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1080400e79-true-d1080478e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/*[not(@xsi:nil = 'true')][not(self::hl7:sequenceNumber[not(@nullFlavor)] | self::hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable])]"
         id="d1080487e11-true-d1080501e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30044-2020-07-21T133700.html"
              test="not(.)">(Dosierung4er)/d1080487e11-true-d1080501e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:sequenceNumber[not(@nullFlavor)] | hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable] (rule-reference: d1080487e11-true-d1080501e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/*[not(@xsi:nil = 'true')][not(self::hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)] | self::hl7:doseQuantity[not(@nullFlavor)] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct])]"
         id="d1080510e8-true-d1080528e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e8-true-d1080528e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)] | hl7:doseQuantity[not(@nullFlavor)] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] (rule-reference: d1080510e8-true-d1080528e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)] | self::hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)] | self::hl7:comp[@xsi:type='PIVL_TS'][@operator='I'])]"
         id="d1080510e43-true-d1080538e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e43-true-d1080538e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)] | hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)] | hl7:comp[@xsi:type='PIVL_TS'][@operator='I'] (rule-reference: d1080510e43-true-d1080538e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:event[not(@nullFlavor)] | self::hl7:offset[not(@nullFlavor)])]"
         id="d1080510e48-true-d1080555e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e48-true-d1080555e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:event[not(@nullFlavor)] | hl7:offset[not(@nullFlavor)] (rule-reference: d1080510e48-true-d1080555e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1080510e80-true-d1080582e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e80-true-d1080582e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1080510e80-true-d1080582e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)]/hl7:phase/*[not(@xsi:nil = 'true')][not(self::hl7:value)]"
         id="d1080510e90-true-d1080596e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e90-true-d1080596e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:value (rule-reference: d1080510e90-true-d1080596e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='I']/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1080510e112-true-d1080623e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e112-true-d1080623e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1080510e112-true-d1080623e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='I']/hl7:phase/*[not(@xsi:nil = 'true')][not(self::hl7:value)]"
         id="d1080510e117-true-d1080637e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e117-true-d1080637e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:value (rule-reference: d1080510e117-true-d1080637e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1080510e138-true-d1080666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e138-true-d1080666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1080510e138-true-d1080666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1080510e140-true-d1080680e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1080510e140-true-d1080680e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1080510e140-true-d1080680e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='COMP'][hl7:supply][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[not(@nullFlavor)][@classCode = 'SPLY'][@moodCode = 'RQO'])]"
         id="d1079107e366-true-d1080697e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e366-true-d1080697e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[not(@nullFlavor)][@classCode = 'SPLY'][@moodCode = 'RQO'] (rule-reference: d1079107e366-true-d1080697e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='COMP'][hl7:supply][not(@nullFlavor)]/hl7:supply[not(@nullFlavor)][@classCode = 'SPLY'][@moodCode = 'RQO']/*[not(@xsi:nil = 'true')][not(self::hl7:independentInd[not(@nullFlavor)] | self::hl7:quantity)]"
         id="d1079107e382-true-d1080716e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e382-true-d1080716e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:independentInd[not(@nullFlavor)] | hl7:quantity (rule-reference: d1079107e382-true-d1080716e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']])]"
         id="d1079107e420-true-d1080739e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e420-true-d1080739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']] (rule-reference: d1079107e420-true-d1080739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | self::hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]])]"
         id="d1080743e29-true-d1080792e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1080743e29-true-d1080792e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]] (rule-reference: d1080743e29-true-d1080792e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1080743e54-true-d1080824e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1080743e54-true-d1080824e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1080743e54-true-d1080824e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']])]"
         id="d1080743e74-true-d1080852e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1080743e74-true-d1080852e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']] (rule-reference: d1080743e74-true-d1080852e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1080743e83-true-d1080892e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1080743e83-true-d1080892e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1080743e83-true-d1080892e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1080743e99-true-d1080920e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1080743e99-true-d1080920e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1080743e99-true-d1080920e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']])]"
         id="d1079107e432-true-d1080944e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1079107e432-true-d1080944e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']] (rule-reference: d1079107e432-true-d1080944e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | self::hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]])]"
         id="d1080948e26-true-d1080997e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1080948e26-true-d1080997e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]] (rule-reference: d1080948e26-true-d1080997e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1080948e48-true-d1081029e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1080948e48-true-d1081029e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1080948e48-true-d1081029e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']])]"
         id="d1080948e68-true-d1081057e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1080948e68-true-d1081057e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']] (rule-reference: d1080948e68-true-d1081057e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1080948e77-true-d1081097e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1080948e77-true-d1081097e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1080948e77-true-d1081097e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1080948e93-true-d1081125e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1080948e93-true-d1081125e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1080948e93-true-d1081125e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']])]"
         id="d1081140e4-true-d1081152e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1081140e4-true-d1081152e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']] (rule-reference: d1081140e4-true-d1081152e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d1081140e17-true-d1081180e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1081140e17-true-d1081180e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d1081140e17-true-d1081180e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1081140e30-true-d1081203e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1081140e30-true-d1081203e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1081140e30-true-d1081203e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']])]"
         id="d1081212e20-true-d1081226e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90013-2020-07-21T133852.html"
              test="not(.)">(Iddescontainers)/d1081212e20-true-d1081226e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']] (rule-reference: d1081212e20-true-d1081226e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument]/hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1'])]"
         id="d1081212e27-true-d1081243e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90013-2020-07-21T133852.html"
              test="not(.)">(Iddescontainers)/d1081212e27-true-d1081243e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1'] (rule-reference: d1081212e27-true-d1081243e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']])]"
         id="d41e24963-true-d1081481e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24963-true-d1081481e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']] (rule-reference: d41e24963-true-d1081481e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4'] | self::hl7:id[1][not(@nullFlavor)] | self::hl7:id[2] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.159-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:text[not(@nullFlavor)] | self::hl7:quantity | self::hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]] | self::hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity] | self::hl7:author[not(@nullFlavor)] | self::hl7:author[@nullFlavor] | self::hl7:entryRelationship[@typeCode='REFR'] | self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']] | self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']] | self::hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']] | self::hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']] | self::hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']] | self::hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']] | self::hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument])]"
         id="d1081254e9-true-d1081668e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e9-true-d1081668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4'] | hl7:id[1][not(@nullFlavor)] | hl7:id[2] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.159-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:text[not(@nullFlavor)] | hl7:quantity | hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]] | hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity] | hl7:author[not(@nullFlavor)] | hl7:author[@nullFlavor] | hl7:entryRelationship[@typeCode='REFR'] | hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']] | hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']] | hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']] | hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']] | hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']] | hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']] | hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument] (rule-reference: d1081254e9-true-d1081668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.159-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1081254e103-true-d1081715e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e103-true-d1081715e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1081254e103-true-d1081715e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1081254e120-true-d1081734e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e120-true-d1081734e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1081254e120-true-d1081734e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']])]"
         id="d1081254e161-true-d1081767e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e161-true-d1081767e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']] (rule-reference: d1081254e161-true-d1081767e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | self::hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']])]"
         id="d1081749e26-true-d1081801e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e26-true-d1081801e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']] (rule-reference: d1081749e26-true-d1081801e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | self::hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] | self::hl7:name | self::pharm:formCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.5' or @nullFlavor] | self::pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']] | self::pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']])]"
         id="d1081749e40-true-d1081867e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e40-true-d1081867e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] | hl7:name | pharm:formCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.5' or @nullFlavor] | pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']] | pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']] (rule-reference: d1081749e40-true-d1081867e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/hl7:code[@codeSystem = '1.2.40.0.34.4.16' or @codeSystem = '1.2.40.0.34.4.17' or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation[@codeSystem = '1.2.40.0.34.4.17' or @nullFlavor])]"
         id="d1081749e56-true-d1081901e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e56-true-d1081901e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation[@codeSystem = '1.2.40.0.34.4.17' or @nullFlavor] (rule-reference: d1081749e56-true-d1081901e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']]/*[not(@xsi:nil = 'true')][not(self::pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE'])]"
         id="d1081749e234-true-d1081937e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e234-true-d1081937e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE'] (rule-reference: d1081749e234-true-d1081937e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:asContent[@classCode = 'CONT'][pharm:containerPackagedMedicine[@classCode = 'CONT'][@determinerCode = 'INSTANCE']]/pharm:containerPackagedMedicine[not(@nullFlavor)][@classCode = 'CONT'][@determinerCode = 'INSTANCE']/*[not(@xsi:nil = 'true')][not(self::pharm:capacityQuantity[not(@nullFlavor)])]"
         id="d1081749e247-true-d1081951e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e247-true-d1081951e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:capacityQuantity[not(@nullFlavor)] (rule-reference: d1081749e247-true-d1081951e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/*[not(@xsi:nil = 'true')][not(self::pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND'])]"
         id="d1081749e280-true-d1081970e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e280-true-d1081970e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND'] (rule-reference: d1081749e280-true-d1081970e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND']/*[not(@xsi:nil = 'true')][not(self::pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor] | self::pharm:name[not(@nullFlavor)])]"
         id="d1081749e316-true-d1081991e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e316-true-d1081991e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor] | pharm:name[not(@nullFlavor)] (rule-reference: d1081749e316-true-d1081991e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:product[hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]]/hl7:manufacturedProduct[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.2.40.0.34.11.2.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[@classCode = 'ACTI'][pharm:ingredient[@classCode = 'MMAT'][@determinerCode = 'KIND']]/pharm:ingredient[not(@nullFlavor)][@classCode = 'MMAT'][@determinerCode = 'KIND']/pharm:code[@codeSystem = '2.16.840.1.113883.6.73' or @codeSystem = '1.2.40.0.34.5.156' or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1081749e325-true-d1082011e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081749e325-true-d1082011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1081749e325-true-d1082011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson])]"
         id="d1081254e188-true-d1082066e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e188-true-d1082066e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] (rule-reference: d1081254e188-true-d1082066e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson[not(@nullFlavor)] | self::hl7:representedOrganization)]"
         id="d1081254e220-true-d1082104e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e220-true-d1082104e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:assignedPerson[not(@nullFlavor)] | hl7:representedOrganization (rule-reference: d1081254e220-true-d1082104e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1082108e42-true-d1082136e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d1082108e42-true-d1082136e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1082108e42-true-d1082136e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1082108e50-true-d1082164e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d1082108e50-true-d1082164e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1082108e50-true-d1082164e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time | self::hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization])]"
         id="d1082190e133-true-d1082224e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e133-true-d1082224e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time | hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization] (rule-reference: d1082190e133-true-d1082224e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization[not(@nullFlavor)])]"
         id="d1082190e180-true-d1082285e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e180-true-d1082285e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:telecom | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization[not(@nullFlavor)] (rule-reference: d1082190e180-true-d1082285e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1082190e244-true-d1082318e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e244-true-d1082318e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1082190e244-true-d1082318e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1082190e255-true-d1082344e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e255-true-d1082344e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1082190e255-true-d1082344e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1082190e277-true-d1082375e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e277-true-d1082375e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1082190e277-true-d1082375e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[@nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:time[@nullFlavor = 'NA'] | self::hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']])]"
         id="d1082190e284-true-d1082414e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e284-true-d1082414e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time[@nullFlavor = 'NA'] | hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']] (rule-reference: d1082190e284-true-d1082414e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:author[@nullFlavor]/hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@nullFlavor = 'NA'])]"
         id="d1082190e302-true-d1082433e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d1082190e302-true-d1082433e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@nullFlavor = 'NA'] (rule-reference: d1082190e302-true-d1082433e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='REFR']/*[not(@xsi:nil = 'true')][not(self::hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']])]"
         id="d1081254e320-true-d1082452e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e320-true-d1082452e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']] (rule-reference: d1081254e320-true-d1082452e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='REFR']/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2'] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct])]"
         id="d1081254e371-true-d1082474e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e371-true-d1082474e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2'] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] (rule-reference: d1081254e371-true-d1082474e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='REFR']/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1081254e395-true-d1082494e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e395-true-d1082494e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1081254e395-true-d1082494e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='REFR']/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1081254e397-true-d1082508e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e397-true-d1082508e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1081254e397-true-d1082508e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | self::hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]])]"
         id="d1082517e29-true-d1082566e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082517e29-true-d1082566e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]] (rule-reference: d1082517e29-true-d1082566e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082517e54-true-d1082598e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082517e54-true-d1082598e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082517e54-true-d1082598e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']])]"
         id="d1082517e74-true-d1082626e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082517e74-true-d1082626e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']] (rule-reference: d1082517e74-true-d1082626e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1082517e83-true-d1082666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082517e83-true-d1082666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1082517e83-true-d1082666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082517e99-true-d1082694e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082517e99-true-d1082694e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082517e99-true-d1082694e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | self::hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]])]"
         id="d1082709e26-true-d1082758e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1082709e26-true-d1082758e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]] (rule-reference: d1082709e26-true-d1082758e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082709e48-true-d1082790e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1082709e48-true-d1082790e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082709e48-true-d1082790e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']])]"
         id="d1082709e68-true-d1082818e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1082709e68-true-d1082818e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']] (rule-reference: d1082709e68-true-d1082818e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1082709e77-true-d1082858e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1082709e77-true-d1082858e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1082709e77-true-d1082858e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082709e93-true-d1082886e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1082709e93-true-d1082886e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082709e93-true-d1082886e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']])]"
         id="d1081254e413-true-d1082910e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e413-true-d1082910e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']] (rule-reference: d1081254e413-true-d1082910e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | self::hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]])]"
         id="d1082914e29-true-d1082963e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082914e29-true-d1082963e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3'] | hl7:code[(@code = 'PINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]] (rule-reference: d1082914e29-true-d1082963e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082914e54-true-d1082995e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082914e54-true-d1082995e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082914e54-true-d1082995e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']])]"
         id="d1082914e74-true-d1083023e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082914e74-true-d1083023e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']] (rule-reference: d1082914e74-true-d1083023e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1082914e83-true-d1083063e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082914e83-true-d1083063e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.161-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1082914e83-true-d1083063e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.49']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.49'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1082914e99-true-d1083091e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30033-2020-10-06T133241.html"
              test="not(.)">(PatientInstructions)/d1082914e99-true-d1083091e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1082914e99-true-d1083091e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']])]"
         id="d1081254e461-true-d1083115e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e461-true-d1083115e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']] (rule-reference: d1081254e461-true-d1083115e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | self::hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]])]"
         id="d1083119e26-true-d1083168e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1083119e26-true-d1083168e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1'] | hl7:code[(@code = 'FINSTRUCT' and @codeSystem = '1.3.6.1.4.1.19376.1.5.3.2')] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]] (rule-reference: d1083119e26-true-d1083168e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1083119e48-true-d1083200e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1083119e48-true-d1083200e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1083119e48-true-d1083200e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']])]"
         id="d1083119e68-true-d1083228e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1083119e68-true-d1083228e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']] (rule-reference: d1083119e68-true-d1083228e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'])]"
         id="d1083119e77-true-d1083268e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1083119e77-true-d1083268e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.160-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] (rule-reference: d1083119e77-true-d1083268e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='SUBJ'][hl7:act/hl7:templateId[@root='2.16.840.1.113883.10.20.1.43']]/hl7:act[hl7:templateId[@root = '2.16.840.1.113883.10.20.1.43'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.3.1']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.0.3.2']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1083119e93-true-d1083296e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30034-2020-10-06T133404.html"
              test="not(.)">(PharmacistInstructions)/d1083119e93-true-d1083296e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1083119e93-true-d1083296e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']])]"
         id="d1083311e4-true-d1083323e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1083311e4-true-d1083323e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']] (rule-reference: d1083311e4-true-d1083323e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d1083311e17-true-d1083351e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1083311e17-true-d1083351e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d1083311e17-true-d1083351e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:act/hl7:templateId[@root='1.2.40.0.34.11.8.1.3.4']]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.4']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.30-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1083311e30-true-d1083374e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30045-2014-09-10T000000.html"
              test="not(.)">(Therapieart)/d1083311e30-true-d1083374e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1083311e30-true-d1083374e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']])]"
         id="d1081254e546-true-d1083472e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1081254e546-true-d1083472e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']] (rule-reference: d1081254e546-true-d1083472e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.21'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.1'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.9'] | self::hl7:effectiveTime[1][hl7:low] | self::hl7:effectiveTime[1][hl7:width] | self::hl7:effectiveTime[1][@nullFlavor] | self::hl7:effectiveTime[hl7:period and not(hl7:phase)] | self::hl7:effectiveTime[2] | self::hl7:effectiveTime[hl7:period and hl7:phase] | self::hl7:effectiveTime[hl7:comp] | self::hl7:effectiveTime[2] | self::hl7:doseQuantity[not(hl7:low|hl7:high)] | self::hl7:doseQuantity[hl7:low|hl7:high] | self::hl7:doseQuantity | self::hl7:doseQuantity[not(hl7:low|hl7:high)] | self::hl7:doseQuantity[hl7:low|hl7:high] | self::hl7:doseQuantity | self::hl7:routeCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.4' or @nullFlavor] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] | self::hl7:entryRelationship[@typeCode='COMP'] | self::hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']] | self::hl7:entryRelationship | self::hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']])]"
         id="d1083384e16-true-d1083581e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1083384e16-true-d1083581e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.21'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.1'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.9'] | hl7:effectiveTime[1][hl7:low] | hl7:effectiveTime[1][hl7:width] | hl7:effectiveTime[1][@nullFlavor] | hl7:effectiveTime[hl7:period and not(hl7:phase)] | hl7:effectiveTime[2] | hl7:effectiveTime[hl7:period and hl7:phase] | hl7:effectiveTime[hl7:comp] | hl7:effectiveTime[2] | hl7:doseQuantity[not(hl7:low|hl7:high)] | hl7:doseQuantity[hl7:low|hl7:high] | hl7:doseQuantity | hl7:doseQuantity[not(hl7:low|hl7:high)] | hl7:doseQuantity[hl7:low|hl7:high] | hl7:doseQuantity | hl7:routeCode[@codeSystem = '1.2.40.0.10.1.4.3.4.3.4' or @nullFlavor] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] | hl7:entryRelationship[@typeCode='COMP'] | hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']] | hl7:entryRelationship | hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']] (rule-reference: d1083384e16-true-d1083581e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[1][hl7:low]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1083621e31-true-d1083641e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30006-2013-12-20T000000.html"
              test="not(.)">(Einnahmedauer)/d1083621e31-true-d1083641e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1083621e31-true-d1083641e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[1][hl7:width]/*[not(@xsi:nil = 'true')][not(self::hl7:width)]"
         id="d1083621e57-true-d1083663e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30006-2013-12-20T000000.html"
              test="not(.)">(Einnahmedauer)/d1083621e57-true-d1083663e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:width (rule-reference: d1083621e57-true-d1083663e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:period and not(hl7:phase)]/*[not(@xsi:nil = 'true')][not(self::hl7:period)]"
         id="d1083675e48-true-d1083685e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30007-2014-09-02T000000.html"
              test="not(.)">(Dosierung1)/d1083675e48-true-d1083685e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:period (rule-reference: d1083675e48-true-d1083685e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:period and hl7:phase]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1083699e78-true-d1083718e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1083699e78-true-d1083718e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1083699e78-true-d1083718e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/*[not(@xsi:nil = 'true')][not(self::hl7:comp[not(@operator)] | self::hl7:comp[@operator='I'])]"
         id="d1083699e103-true-d1083737e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1083699e103-true-d1083737e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:comp[not(@operator)] | hl7:comp[@operator='I'] (rule-reference: d1083699e103-true-d1083737e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/hl7:comp[not(@operator)]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1083699e119-true-d1083754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1083699e119-true-d1083754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1083699e119-true-d1083754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:effectiveTime[hl7:comp]/hl7:comp[@operator='I']/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1083699e138-true-d1083781e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30009-2015-09-05T000000.html"
              test="not(.)">(Dosierung3)/d1083699e138-true-d1083781e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1083699e138-true-d1083781e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:doseQuantity[hl7:low|hl7:high]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1083800e38-true-d1083822e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30037-2015-09-05T000000.html"
              test="not(.)">(Dosierung1dq)/d1083800e38-true-d1083822e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1083800e38-true-d1083822e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:doseQuantity[hl7:low|hl7:high]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1083843e38-true-d1083865e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30041-2015-09-05T000000.html"
              test="not(.)">(Dosierung3dq)/d1083843e38-true-d1083865e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1083843e38-true-d1083865e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1083384e77-true-d1083902e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1083384e77-true-d1083902e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1083384e77-true-d1083902e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1083384e79-true-d1083916e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1083384e79-true-d1083916e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1083384e79-true-d1083916e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/*[not(@xsi:nil = 'true')][not(self::hl7:sequenceNumber[not(@nullFlavor)] | self::hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable])]"
         id="d1083930e11-true-d1083944e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30040-2020-07-21T133602.html"
              test="not(.)">(Dosierung2er)/d1083930e11-true-d1083944e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:sequenceNumber[not(@nullFlavor)] | hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable] (rule-reference: d1083930e11-true-d1083944e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/*[not(@xsi:nil = 'true')][not(self::hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)] | self::hl7:doseQuantity[not(@nullFlavor)] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct])]"
         id="d1083953e8-true-d1083971e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1083953e8-true-d1083971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)] | hl7:doseQuantity[not(@nullFlavor)] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] (rule-reference: d1083953e8-true-d1083971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='EIVL_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:event[not(@nullFlavor)] | self::hl7:offset[not(@nullFlavor)])]"
         id="d1083953e17-true-d1083988e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1083953e17-true-d1083988e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:event[not(@nullFlavor)] | hl7:offset[not(@nullFlavor)] (rule-reference: d1083953e17-true-d1083988e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1083953e77-true-d1084017e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1083953e77-true-d1084017e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1083953e77-true-d1084017e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='EIVL_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1083953e79-true-d1084031e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30046-2015-09-05T000000.html"
              test="not(.)">(Splitdose1)/d1083953e79-true-d1084031e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1083953e79-true-d1084031e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/*[not(@xsi:nil = 'true')][not(self::hl7:sequenceNumber[not(@nullFlavor)] | self::hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable])]"
         id="d1084047e11-true-d1084061e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30044-2020-07-21T133700.html"
              test="not(.)">(Dosierung4er)/d1084047e11-true-d1084061e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:sequenceNumber[not(@nullFlavor)] | hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable] (rule-reference: d1084047e11-true-d1084061e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/*[not(@xsi:nil = 'true')][not(self::hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)] | self::hl7:doseQuantity[not(@nullFlavor)] | self::hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct])]"
         id="d1084070e8-true-d1084088e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e8-true-d1084088e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)] | hl7:doseQuantity[not(@nullFlavor)] | hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] (rule-reference: d1084070e8-true-d1084088e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)] | self::hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)] | self::hl7:comp[@xsi:type='PIVL_TS'][@operator='I'])]"
         id="d1084070e43-true-d1084098e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e43-true-d1084098e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)] | hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)] | hl7:comp[@xsi:type='PIVL_TS'][@operator='I'] (rule-reference: d1084070e43-true-d1084098e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='EIVL_TS'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:event[not(@nullFlavor)] | self::hl7:offset[not(@nullFlavor)])]"
         id="d1084070e48-true-d1084115e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e48-true-d1084115e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:event[not(@nullFlavor)] | hl7:offset[not(@nullFlavor)] (rule-reference: d1084070e48-true-d1084115e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1084070e80-true-d1084142e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e80-true-d1084142e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1084070e80-true-d1084142e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='A'][not(@nullFlavor)]/hl7:phase/*[not(@xsi:nil = 'true')][not(self::hl7:value)]"
         id="d1084070e90-true-d1084156e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e90-true-d1084156e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:value (rule-reference: d1084070e90-true-d1084156e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='I']/*[not(@xsi:nil = 'true')][not(self::hl7:phase | self::hl7:period)]"
         id="d1084070e112-true-d1084183e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e112-true-d1084183e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:phase | hl7:period (rule-reference: d1084070e112-true-d1084183e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:effectiveTime[@xsi:type='SXPR_TS'][not(@nullFlavor)]/hl7:comp[@xsi:type='PIVL_TS'][@operator='I']/hl7:phase/*[not(@xsi:nil = 'true')][not(self::hl7:value)]"
         id="d1084070e117-true-d1084197e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e117-true-d1084197e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:value (rule-reference: d1084070e117-true-d1084197e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']])]"
         id="d1084070e138-true-d1084226e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e138-true-d1084226e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] (rule-reference: d1084070e138-true-d1084226e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:entryRelationship[@typeCode='COMP'][hl7:substanceAdministration/hl7:templateId[@root='1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:substanceAdministration[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]/hl7:entryRelationship[hl7:substanceAdministration/hl7:effectiveTime[@xsi:type='SXPR_TS']]/hl7:substanceAdministration[not(@nullFlavor)][@classCode = 'SBADM'][@moodCode = 'INT'][hl7:consumable]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedMaterial[@nullFlavor = 'NA'])]"
         id="d1084070e140-true-d1084240e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.30047-2015-09-05T000000.html"
              test="not(.)">(Splitdose2)/d1084070e140-true-d1084240e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedMaterial[@nullFlavor = 'NA'] (rule-reference: d1084070e140-true-d1084240e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']])]"
         id="d1084249e20-true-d1084263e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90013-2020-07-21T133852.html"
              test="not(.)">(Iddescontainers)/d1084249e20-true-d1084263e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']] (rule-reference: d1084249e20-true-d1084263e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]/hl7:reference[@typeCode = 'XCRPT'][hl7:externalDocument]/hl7:externalDocument[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1'])]"
         id="d1084249e27-true-d1084280e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90013-2020-07-21T133852.html"
              test="not(.)">(Iddescontainers)/d1084249e27-true-d1084280e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.1'] (rule-reference: d1084249e27-true-d1084280e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/*[not(@xsi:nil = 'true')][not(self::hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']])]"
         id="d41e24975-true-d1084590e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24975-true-d1084590e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']] (rule-reference: d41e24975-true-d1084590e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50'] | self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.191-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:statusCode[@code = 'active'] | self::hl7:effectiveTime[1] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1084291e9-true-d1084933e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e9-true-d1084933e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50'] | hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.191-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:statusCode[@code = 'active'] | hl7:effectiveTime[1] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1084291e9-true-d1084933e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.191-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1084291e22-true-d1084963e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e22-true-d1084963e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1084291e22-true-d1084963e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.191-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1084967e41-true-d1084979e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d1084967e41-true-d1084979e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1084967e41-true-d1084979e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:effectiveTime[1]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1084291e49-true-d1085007e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e49-true-d1085007e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1084291e49-true-d1085007e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1084291e63-true-d1085144e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e63-true-d1085144e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1084291e63-true-d1085144e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1085022e41-true-d1085252e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085022e41-true-d1085252e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1085022e41-true-d1085252e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1085022e60-true-d1085311e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085022e60-true-d1085311e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1085022e60-true-d1085311e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1085022e106-true-d1085368e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085022e106-true-d1085368e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1085022e106-true-d1085368e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1085372e92-true-d1085402e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1085372e92-true-d1085402e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1085372e92-true-d1085402e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1085022e129-true-d1085450e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085022e129-true-d1085450e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1085022e129-true-d1085450e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1085022e145-true-d1085495e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085022e145-true-d1085495e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1085022e145-true-d1085495e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1085465e67-true-d1085558e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085465e67-true-d1085558e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1085465e67-true-d1085558e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1084291e65-true-d1085726e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e65-true-d1085726e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1084291e65-true-d1085726e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1085603e15-true-d1085858e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085603e15-true-d1085858e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1085603e15-true-d1085858e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1085731e50-true-d1085924e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085731e50-true-d1085924e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1085731e50-true-d1085924e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1085731e120-true-d1085986e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085731e120-true-d1085986e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1085731e120-true-d1085986e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1085731e132-true-d1086008e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085731e132-true-d1086008e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1085731e132-true-d1086008e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1085996e12-true-d1086037e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085996e12-true-d1086037e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1085996e12-true-d1086037e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1085731e143-true-d1086088e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085731e143-true-d1086088e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1085731e143-true-d1086088e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1086062e58-true-d1086149e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086062e58-true-d1086149e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1086062e58-true-d1086149e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1085603e17-true-d1086226e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085603e17-true-d1086226e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1085603e17-true-d1086226e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1085603e26-true-d1086279e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085603e26-true-d1086279e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1085603e26-true-d1086279e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1085603e30-true-d1086334e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1085603e30-true-d1086334e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1085603e30-true-d1086334e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1086327e10-true-d1086361e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086327e10-true-d1086361e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1086327e10-true-d1086361e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1084291e77-true-d1086415e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1084291e77-true-d1086415e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1084291e77-true-d1086415e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1086392e6-true-d1086448e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086392e6-true-d1086448e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1086392e6-true-d1086448e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:entry[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.50']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1086468e54-true-d1086480e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1086468e54-true-d1086480e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1086468e54-true-d1086480e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d41e24988-true-d1086775e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d41e24988-true-d1086775e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d41e24988-true-d1086775e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1086496e8-true-d1087088e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086496e8-true-d1087088e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1086496e8-true-d1087088e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1086496e59-true-d1087242e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086496e59-true-d1087242e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1086496e59-true-d1087242e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1087120e45-true-d1087350e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087120e45-true-d1087350e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1087120e45-true-d1087350e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1087120e64-true-d1087409e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087120e64-true-d1087409e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1087120e64-true-d1087409e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1087120e110-true-d1087466e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087120e110-true-d1087466e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1087120e110-true-d1087466e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1087470e92-true-d1087500e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1087470e92-true-d1087500e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1087470e92-true-d1087500e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1087120e133-true-d1087548e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087120e133-true-d1087548e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1087120e133-true-d1087548e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1087120e149-true-d1087593e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087120e149-true-d1087593e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1087120e149-true-d1087593e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1087563e67-true-d1087656e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087563e67-true-d1087656e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1087563e67-true-d1087656e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1086496e65-true-d1087824e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1086496e65-true-d1087824e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1086496e65-true-d1087824e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1087701e5-true-d1087956e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087701e5-true-d1087956e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1087701e5-true-d1087956e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1087829e50-true-d1088022e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087829e50-true-d1088022e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1087829e50-true-d1088022e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1087829e120-true-d1088084e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087829e120-true-d1088084e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1087829e120-true-d1088084e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1087829e132-true-d1088106e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087829e132-true-d1088106e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1087829e132-true-d1088106e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1088094e12-true-d1088135e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1088094e12-true-d1088135e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1088094e12-true-d1088135e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1087829e143-true-d1088186e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087829e143-true-d1088186e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1087829e143-true-d1088186e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1088160e58-true-d1088247e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1088160e58-true-d1088247e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1088160e58-true-d1088247e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1087701e7-true-d1088324e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087701e7-true-d1088324e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1087701e7-true-d1088324e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1087701e16-true-d1088377e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087701e16-true-d1088377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1087701e16-true-d1088377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1087701e20-true-d1088432e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1087701e20-true-d1088432e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1087701e20-true-d1088432e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.63']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1088425e10-true-d1088459e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.2.63-2019-11-27T133537.html"
              test="not(.)">(atcdabbr_section_MedikationslistePSKodiert)/d1088425e10-true-d1088459e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1088425e10-true-d1088459e0)</assert>
   </rule>
</pattern>
