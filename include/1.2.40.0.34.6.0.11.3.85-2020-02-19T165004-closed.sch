<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.85
Name: Device Time Synchronization Information Observation
Description: Die Device Time Synchronization Information Observation dokumentiert die Zeit-Synchronisierung Methode des verwendeten Gerätes. In PCHA konformen Geräten, kommen die Informationen aus dem MdsTimeInfo Attribut.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.85-2020-02-19T165004-closed">
   <title>Device Time Synchronization Information Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']])]"
         id="d41e55040-true-d3609712e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2020-02-19T165004.html"
              test="not(.)">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)/d41e55040-true-d3609712e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']] (rule-reference: d41e55040-true-d3609712e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17'] | self::hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)][hl7:reference] | self::hl7:value[@codeSystem = '2.16.840.1.113883.6.24'])]"
         id="d41e55097-true-d3609756e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2020-02-19T165004.html"
              test="not(.)">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)/d41e55097-true-d3609756e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17'] | hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)][hl7:reference] | hl7:value[@codeSystem = '2.16.840.1.113883.6.24'] (rule-reference: d41e55097-true-d3609756e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:code[(@code = '68220' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e55123-true-d3609788e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2020-02-19T165004.html"
              test="not(.)">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)/d41e55123-true-d3609788e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e55123-true-d3609788e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e55158-true-d3609812e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.85-2020-02-19T165004.html"
              test="not(.)">(atcdabbr_entry_DeviceTimeSynchronizationInformationObservation)/d41e55158-true-d3609812e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e55158-true-d3609812e0)</assert>
   </rule>
</pattern>
