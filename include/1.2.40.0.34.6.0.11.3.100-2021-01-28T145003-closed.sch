<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.100
Name: Serienmessung Vitalparameter Entry
Description: Das Serienmessung Entry dokumentiert eine kontinuierliche Messungen eines Gerätes. Eine kontinuierliche Messung beinhaltet mehrere Datenpunkte und eine Zeitspanne. Diese Messwerte sind   als Vitalparameter definiert! 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003-closed">
   <title>Serienmessung Vitalparameter Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']])]"
         id="d41e33242-true-d2284423e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33242-true-d2284423e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']] (rule-reference: d41e33242-true-d2284423e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[@value] | self::hl7:effectiveTime[@nullFlavor='UNK'] | self::hl7:effectiveTime | self::hl7:value[@nullFlavor='NA'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]])]"
         id="d41e33343-true-d2284989e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33343-true-d2284989e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | hl7:id[not(@nullFlavor)] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[@value] | hl7:effectiveTime[@nullFlavor='UNK'] | hl7:effectiveTime | hl7:value[@nullFlavor='NA'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]] (rule-reference: d41e33343-true-d2284989e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation | self::ips:designation)]"
         id="d41e33391-true-d2285039e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33391-true-d2285039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation | ips:designation (rule-reference: d41e33391-true-d2285039e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e33409-true-d2285053e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33409-true-d2285053e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e33409-true-d2285053e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e33448-true-d2285082e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33448-true-d2285082e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e33448-true-d2285082e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d41e33506-true-d2285114e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33506-true-d2285114e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d41e33506-true-d2285114e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d41e33579-true-d2285283e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33579-true-d2285283e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d41e33579-true-d2285283e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2285144e14-true-d2285389e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285144e14-true-d2285389e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2285144e14-true-d2285389e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2285393e101-true-d2285462e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285393e101-true-d2285462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2285393e101-true-d2285462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2285393e174-true-d2285538e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2285393e174-true-d2285538e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2285393e174-true-d2285538e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2285393e186-true-d2285560e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2285393e186-true-d2285560e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2285393e186-true-d2285560e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2285548e12-true-d2285589e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d2285548e12-true-d2285589e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2285548e12-true-d2285589e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2285393e198-true-d2285640e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285393e198-true-d2285640e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2285393e198-true-d2285640e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2285614e58-true-d2285701e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285614e58-true-d2285701e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2285614e58-true-d2285701e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d41e33581-true-d2285868e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33581-true-d2285868e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d41e33581-true-d2285868e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d2285746e52-true-d2285976e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285746e52-true-d2285976e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d2285746e52-true-d2285976e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2285746e71-true-d2286035e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285746e71-true-d2286035e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2285746e71-true-d2286035e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2285746e117-true-d2286092e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285746e117-true-d2286092e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2285746e117-true-d2286092e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2286096e92-true-d2286126e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d2286096e92-true-d2286126e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2286096e92-true-d2286126e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d2285746e140-true-d2286174e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285746e140-true-d2286174e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d2285746e140-true-d2286174e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2285746e156-true-d2286219e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2285746e156-true-d2286219e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2285746e156-true-d2286219e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2286189e67-true-d2286282e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286189e67-true-d2286282e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2286189e67-true-d2286282e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d41e33594-true-d2286450e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33594-true-d2286450e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d41e33594-true-d2286450e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2286327e5-true-d2286582e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286327e5-true-d2286582e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2286327e5-true-d2286582e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2286455e50-true-d2286648e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286455e50-true-d2286648e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2286455e50-true-d2286648e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2286455e120-true-d2286710e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286455e120-true-d2286710e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2286455e120-true-d2286710e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2286455e132-true-d2286732e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286455e132-true-d2286732e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2286455e132-true-d2286732e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2286720e12-true-d2286761e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286720e12-true-d2286761e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2286720e12-true-d2286761e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2286455e143-true-d2286812e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286455e143-true-d2286812e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2286455e143-true-d2286812e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2286786e58-true-d2286873e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286786e58-true-d2286873e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2286786e58-true-d2286873e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d2286327e7-true-d2286950e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286327e7-true-d2286950e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d2286327e7-true-d2286950e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2286327e16-true-d2287003e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286327e16-true-d2287003e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2286327e16-true-d2287003e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2286327e20-true-d2287058e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2286327e20-true-d2287058e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2286327e20-true-d2287058e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2287051e10-true-d2287085e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287051e10-true-d2287085e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2287051e10-true-d2287085e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d41e33596-true-d2287213e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33596-true-d2287213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d41e33596-true-d2287213e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d2287116e11-true-d2287321e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287116e11-true-d2287321e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d2287116e11-true-d2287321e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2287116e22-true-d2287383e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287116e22-true-d2287383e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2287116e22-true-d2287383e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d2287116e78-true-d2287461e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287116e78-true-d2287461e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d2287116e78-true-d2287461e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d2287116e80-true-d2287522e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287116e80-true-d2287522e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d2287116e80-true-d2287522e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d2287116e83-true-d2287580e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287116e83-true-d2287580e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d2287116e83-true-d2287580e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']])]"
         id="d41e33598-true-d2287635e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d41e33598-true-d2287635e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']] (rule-reference: d41e33598-true-d2287635e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | self::hl7:code[@nullFlavor='NA'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]])]"
         id="d2287603e6-true-d2287692e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287603e6-true-d2287692e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | hl7:code[@nullFlavor='NA'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]] (rule-reference: d2287603e6-true-d2287692e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']])]"
         id="d2287603e36-true-d2287730e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287603e36-true-d2287730e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']] (rule-reference: d2287603e36-true-d2287730e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | self::hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value)]"
         id="d2287712e7-true-d2287779e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287712e7-true-d2287779e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:value (rule-reference: d2287712e7-true-d2287779e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:translation | self::ips:designation)]"
         id="d2287712e31-true-d2287823e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287712e31-true-d2287823e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:translation | ips:designation (rule-reference: d2287712e31-true-d2287823e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2287712e48-true-d2287837e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287712e48-true-d2287837e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2287712e48-true-d2287837e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2287712e102-true-d2287872e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287712e102-true-d2287872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2287712e102-true-d2287872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:origin[not(@nullFlavor)] | self::hl7:scale[not(@nullFlavor)] | self::hl7:digits[not(@nullFlavor)])]"
         id="d2287712e119-true-d2287907e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287712e119-true-d2287907e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:origin[not(@nullFlavor)] | hl7:scale[not(@nullFlavor)] | hl7:digits[not(@nullFlavor)] (rule-reference: d2287712e119-true-d2287907e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']])]"
         id="d2287603e46-true-d2287945e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287603e46-true-d2287945e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']] (rule-reference: d2287603e46-true-d2287945e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | self::hl7:code[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)])]"
         id="d2287927e7-true-d2287985e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287927e7-true-d2287985e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | hl7:code[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] (rule-reference: d2287927e7-true-d2287985e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2287927e41-true-d2288016e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287927e41-true-d2288016e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2287927e41-true-d2288016e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:head[not(@nullFlavor)] | self::hl7:increment[not(@nullFlavor)])]"
         id="d2287927e53-true-d2288040e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d2287927e53-true-d2288040e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:head[not(@nullFlavor)] | hl7:increment[not(@nullFlavor)] (rule-reference: d2287927e53-true-d2288040e0)</assert>
   </rule>
</pattern>
