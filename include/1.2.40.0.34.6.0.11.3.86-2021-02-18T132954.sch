<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.86
Name: Device Protocol Information Observation
Description: Die Device Protocol Information Observation dokumentiert das interne Protokoll des verwendeten Gerätes. Bei PCHA konformen Geräten, kommen die Informationen aus dem Production specification Attribut.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954">
   <title>Device Protocol Information Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]"
         id="d41e55226-false-d3609863e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86']) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86']) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86']
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86']"
         id="d41e55232-false-d3609932e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.86')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.86' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']"
         id="d41e55241-false-d3609947e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.17')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.17' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']"
         id="d41e55252-false-d3609969e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.30')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.30' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e55264-false-d3609991e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@code) = ('531977')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von code MUSS '531977' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="string(@displayName) = ('MDC_ID_PROD_SPEC_PROTOCOL: Protocol Information')">(atcdabbr_entry_DeviceProtocolInformationObservation): Der Wert von displayName MUSS 'MDC_ID_PROD_SPEC_PROTOCOL: Protocol Information' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e55274-false-d3610029e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="@code">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="@codeSystem">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:code[(@code = '531977' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e55289-false-d3610056e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="@language">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]"
         id="d41e55298-false-d3610072e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceProtocolInformationObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]"
         id="d41e55303-false-d3610091e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="@value">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceProtocolInformationObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.86
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]
Item: (atcdabbr_entry_DeviceProtocolInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]"
         id="d41e55309-false-d3610107e0">
      <extends rule="ST"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.html"
              test="@nullFlavor or ($xsiLocalName='ST' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceProtocolInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
